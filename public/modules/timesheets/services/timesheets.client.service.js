'use strict';

/*globals moment, _ */
//Timesheets service used to communicate Timesheets REST endpoints
angular.module('timesheets').factory('Timesheets', ['$resource',
	function($resource) {
		return $resource('timesheets/:timesheetId', { timesheetId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
])
.factory('TimesheetUtils', ['coreConfig', function(coreConfig){

    /*
     * based on option return all the possible periods.
     */
    var getPeriods = function (opt){
        var interval,startDate;
        if(opt.period.toLowerCase() === 'weekly'){
            interval = {tick:7, unit:'days'};
            //Set up Start Date
            startDate = moment(opt.joinDate).day(opt.startDay).format('YYYY-MM-DD');
            if(startDate > opt.joinDate){
                startDate = moment(moment(startDate).add(-interval.tick,interval.unit));
            }
        }
        var period = [];
        var currDate = moment(startDate);
        while(currDate.valueOf() <= moment(opt.endDate).valueOf()){
            var tmpDate = moment(currDate);
            currDate.add(interval.tick,interval.unit);
            var timesheet = {
                start: tmpDate.format('YYYY-MM-DD'),
                end: moment(currDate).add(-1,'day').format('YYYY-MM-DD'),
                project: opt.project,
                user:opt.user,
                status: 'New'
            };
            period.push(timesheet);
        }
        return period;
    };

    return {
        /*
         * based on users option generate timesheets and fill the exisiting timeseets
         * output will have all the timesheets with empty new etc status.
         */
        generate:function(user){
            var projects=user.projects, timesheets=user.timesheets;
            projects.forEach(function(el){
                //TODO: options data concestancy.
                var opt = {
                    project:el.project._id,
                    user: user._id,
                    joinDate:el.startDate.substring(0,10),
                    period: el.project.timesheetCycle || el.project.invoiceCycle,
                    startDay: el.project.weekStartDay || el.project.weekStartDay, //Sunday or Monday
                    endDate: el.endDate?el.endDate.substring(0,10) : coreConfig.date
                };

                var periods = getPeriods(opt);

                for(var i=-1;++i<timesheets.length;){
                    var ind = _.findIndex(periods, { 'start': timesheets[i].start });
                    periods[ind] = timesheets[i];
                }

                el.timesheets = periods;
            });
        },
        timesheets:{}
    };
}]);
