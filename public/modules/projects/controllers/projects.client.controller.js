'use strict';

// Projects controller
angular.module('projects').controller('ProjectsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Projects','Vendors','globalLists',
	function($scope, $stateParams, $location, Authentication, Projects, Vendors, globalLists) {

        $scope.authentication = Authentication;
        $scope.globalLists = globalLists;
        $scope.vendors = Vendors.getList();
        $scope.project = $scope.project || {};

        // Create new Project
		$scope.create = function() {

            if($scope.action==='Edit'){
                return $scope.update();
            }

			// Create new Project object
			var project = new Projects (this.project);

            if(project.vendor && project.vendor._id){
                project.vendor = project.vendor._id;
            }
			// Redirect after save
			project.$save(function(response) {
				$location.path('projects/' + response._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});

			// Clear form fields
			this.name = '';
		};

		// Remove existing Project
		$scope.remove = function( project ) {
			if ( project ) { project.$remove();

				for (var i in $scope.projects ) {
					if ($scope.projects [i] === project ) {
						$scope.projects.splice(i, 1);
					}
				}
			} else {
				$scope.project.$remove(function() {
					$location.path('projects');
				});
			}
		};

		// Update existing Project
		$scope.update = function() {
			var project = $scope.project ;
			project.$update(function() {
				$location.path('projects/' + project._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Projects
		$scope.find = function() {
			$scope.projects = Projects.query();
		};

		// Find existing Project
		$scope.findOne = function() {
            $scope.action = 'New';
            if($stateParams.projectId){
                $scope.action = 'Edit';
                $scope.project = Projects.get({
                    projectId: $stateParams.projectId
                });
            }
		};
	}
]);