'use strict';


angular.module('core').controller('HomeController', ['$scope', 'Authentication','$location',
	function($scope, Authentication,$location) {
        if($location.url()==='/'){
            if (Authentication.user){
                $location.path('/home');
            } else {
                $location.path('/signin');
            }
        }
		// This provides Authentication context.
		$scope.authentication = Authentication;
	}
]);