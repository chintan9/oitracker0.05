'use strict';

module.exports = function(app) {
	var users = require('../controllers/users.server.controller');
	var expenses = require('../controllers/expenses.server.controller');
	var fileUpload = require('../modules/file.upload');
	var mongoose = require('mongoose');
	var Model = mongoose.model('Expense');
	// Expenses Routes
	app.route('/expenses')
		.get(expenses.list)
		.post(users.requiresLogin, expenses.create);

    // expense Upload Routes
    app.route('/expenses/upload')
        .post(fileUpload.upload(Model));

    app.route('/expenses/deleteDoc')
        .post(fileUpload.deleteDoc(Model));
    
	app.route('/expenses/:expenseId')
		.get(expenses.read)
		.put(users.requiresLogin, expenses.hasAuthorization, expenses.update)
		.delete(users.requiresLogin, expenses.hasAuthorization, expenses.delete);

	// Finish by binding the Expense middleware
	app.param('expenseId', expenses.expenseByID);
};