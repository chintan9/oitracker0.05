'use strict';

module.exports = function(app) {
	var users = require('../controllers/users.server.controller');
	var invoices = require('../controllers/invoices.server.controller');

	// Invoices Routes
	app.route('/invoices')
		.get(users.hasAuthorization(['admin']),invoices.list)
		.post(users.hasAuthorization(['admin']), invoices.create);

	app.route('/invoices/:invoiceId')
		.get(users.hasAuthorization(['admin']),invoices.read)
		.put(users.hasAuthorization(['admin']), invoices.update)
		.delete(users.hasAuthorization(['admin']), invoices.delete);

	// Finish by binding the Invoice middleware
	app.param('invoiceId', invoices.invoiceByID);
};