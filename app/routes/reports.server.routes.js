'use strict';

module.exports = function(app) {
	var users = require('../controllers/users.server.controller');
	var reports = require('../controllers/reports.server.controller');

	// Invoices Routes
	app.route('/reports/timesheets')
		.post(users.hasAuthorization(['admin']), reports.timesheets);

};