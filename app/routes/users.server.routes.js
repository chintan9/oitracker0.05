'use strict';

/**
 * Module dependencies.
 */
var passport = require('passport');


module.exports = function(app) {
	// User Routes
	var users = require('../controllers/users.server.controller');
	var fileUpload = require('../modules/file.upload');
	var mongoose = require('mongoose');
	var Model = mongoose.model('User');

	// Setting up the users profile api
	app.route('/users/me').get(users.me);

    app.route('/users/data').post(users.hasAuthorization(['admin']), users.getData).get(users.getData);

    app.route('/users/accounts').delete(users.removeOAuthProvider);

    // Setting up the users password api
    app.route('/users/password').post(users.changePassword);

    // Users Upload Routes
    app.route('/users/upload')
        .post(fileUpload.upload(Model));
	app.route('/users/download/:modelId/*')
		.get(fileUpload.download(Model));

	app.route('/users/deleteDoc')
        .post(fileUpload.deleteDoc(Model));

	app.route('/users/getList')
		.get(users.hasAuthorization(['admin']),users.getList);

    app.route('/users/:userId')
        .get(users.hasAuthorization(['admin']),users.read)
        .put( users.change);

	app.route('/users')
        .put(users.update)
        .get(users.hasAuthorization(['admin']), users.list)
        .post(users.hasAuthorization(['admin']), users.create);

	// Setting up the users password api
	app.route('/users/password').post(users.changePassword);
	app.route('/auth/forgot').post(users.forgot);
	app.route('/auth/reset/:token').get(users.validateResetToken);
	app.route('/auth/reset/:token').post(users.reset);

	// Setting up the users authentication api
	app.route('/auth/signup').post(users.signup);
	app.route('/auth/signin').post(users.signin);
	app.route('/auth/signout').get(users.signout);

	// Setting the facebook oauth routes
	app.route('/auth/facebook').get(passport.authenticate('facebook', {
		scope: ['email']
	}));
	app.route('/auth/facebook/callback').get(users.oauthCallback('facebook'));

	// Setting the twitter oauth routes
	app.route('/auth/twitter').get(passport.authenticate('twitter'));
	app.route('/auth/twitter/callback').get(users.oauthCallback('twitter'));

	// Setting the google oauth routes
	app.route('/auth/google').get(passport.authenticate('google', {
		scope: [
			'https://www.googleapis.com/auth/userinfo.profile',
			'https://www.googleapis.com/auth/userinfo.email'
		]
	}));
	app.route('/auth/google/callback').get(users.oauthCallback('google'));

	// Setting the linkedin oauth routes
	app.route('/auth/linkedin').get(passport.authenticate('linkedin'));
	app.route('/auth/linkedin/callback').get(users.oauthCallback('linkedin'));

	// Setting the github oauth routes
	app.route('/auth/github').get(passport.authenticate('github'));
	app.route('/auth/github/callback').get(users.oauthCallback('github'));


    app.route('/testCall').get(users.testCall);


    // Finish by binding the user middleware
	app.param('userId', users.userByID);
};
