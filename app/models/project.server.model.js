'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;
var TrimmedString = {type:String, trim:true};

/**
 * Project Schema
 */
var ProjectSchema = new Schema({
	name: { type: String, required: 'Please fill Project name',trim: true},
    client: { type: String, required: 'Please fill Client Name',trim: true},
    location: {
        line1: TrimmedString,
        line2:TrimmedString,
        city: TrimmedString,
        zip: TrimmedString,
        state: TrimmedString,
        country: TrimmedString
    },
    docs: [{
        type: TrimmedString,
        file: TrimmedString,
		s3error: Boolean,
        _id:false
    }],
    vendor: {
        type: Schema.ObjectId,
        ref: 'Vendor'
    },
    timesheetCycle:TrimmedString,
    weekStartDay:TrimmedString, /* If weekly need to identify Sun/Mon etc*/
    created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

ProjectSchema.statics.getList = function(callback){
    this.find({},'_id name vendor').sort('name').lean(true).exec(function(err,projects){
        if(err){
            return callback(err);
        } else {
            callback(undefined, projects);
        }
    });
};

mongoose.model('Project', ProjectSchema);